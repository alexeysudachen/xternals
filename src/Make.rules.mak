
.SUFFIXES: .c .cc .cxx .obj .lib .dll .exe .S

!if "$(CPU)" == "" || ("$(CPU)" != "X64" && "$(CPU)" != "AMD64")
CPU = X86
LIBDIR = $(TOPDIR)\..\lib
BINDIR = $(TOPDIR)\..\bin
!else
CPU = X64
LIBDIR = $(TOPDIR)\..\lib\amd64
BINDIR = $(TOPDIR)\..\bin\amd64
!endif

LIB=$(LIB) $(LIBDIR)
INCLUDE=$(INCLUDE) $(TOPDIR)\..\include

!if "$(TEMP_BUILD)"==""
TEMP_BUILD=$(TEMP)
!endif

!if "$(COMPIGEN)"==""
COMPIGEN=99
!endif

NOLOGO = -nologo
_CCFLAGS = /Gy /FC /GF /Zi
_CCXFLAGS = $(_CCFLAGS) /EHsc /GR -wd4530

!if "$(STATIC)" == "YES"
STATIC_LIB = YES
LINKAGE = Static
LINKSFX = _Lib
!if "$(DEBUG)" != "YES"
DBGSFX = 
LIBSFX = -mt$(COMPIGEN)
_ECCFLAGS = /O2 /Oy- /MT
CONFIG=Release
!else
DBGSFX = _Dbg
LIBSFX = -mt$(COMPIGEN)d
_ECCFLAGS = /Od /GS /RTC1 /D_DEBUG /MTd
CONFIG=Debug
!endif
!else
STATIC_LIB = NO
LINKAGE = Dynamic
LINKSFX = _DLL
!if "$(DEBUG)" != "YES"
DBGSFX = 
LIBSFX = -md$(COMPIGEN)
DLLSFX = -$(CPU)-vc$(COMPIGEN)0
_ECCFLAGS = /O1 /Oy- /GL /MD
LTCG=/LTCG
CONFIG=Release
!else
DBGSFX = _Dbg
LIBSFX = -md$(COMPIGEN)d
DLLSFX = -$(CPU)-vc$(COMPIGEN)0d
_ECCFLAGS = /Od /GS /RTC1 /D_DEBUG /MDd
CONFIG=Debug
!endif
!endif

DLLNAME=$(BINDIR)\$(PROJECT)$(DLLSFX).dll
LIBNAME=$(LIBDIR)\$(PROJECT)$(LIBSFX).lib
EXPNAME=$(LIBDIR)\$(PROJECT)$(LIBSFX).exp

!if "$(STATIC)" == "YES" || "$(NO_DLL_TARGET)" == "YES"
PDBNAME=$(LIBDIR)\$(PROJECT)$(LIBSFX).pdb
!else
PDBNAME=$(BINDIR)\$(PROJECT)$(DLLSFX).pdb
!endif

CCFLAGS = $(_CCFLAGS) $(_ECCFLAGS)
CCXFLAGS = $(_CCXFLAGS) $(_ECCFLAGS)

TMPDIR = $(TEMP_BUILD)\XTERNAL\$(PROJECT)_MSVC$(COMPIGEN)_$(CPU)$(DBGSFX)$(LINKSFX)
SRCDIR = $(TEMP_BUILD)\XTERNAL\S\$(PROJECT)
OBJDIR = $(TMPDIR)\obj

!if "$(TARGET)" == ""
!if "$(STATIC)" == "YES" || "$(NO_DLL_TARGET)" == "YES"
_ECCFLAGS= $(_ECCFLAGS) /Fd$(PDBNAME)
!else
_ECCFLAGS= $(_ECCFLAGS) /Fd$(TMPDIR)\$(PROJECT).pdb
!endif
!endif

CC = cl $(NOLOGO)
CCX = cl $(NOLOGO)

LIBLINK = link /lib /machine:$(CPU) $(NOLOGO)
DLLLINK = link /dll /debug /machine:$(CPU) $(NOLOGO) /opt:icf=10 /opt:ref /incremental:no $(LTCG)

!if "$(TARGET)" == ""
!if "$(STATIC)" == "YES" || "$(NO_DLL_TARGET)" == "YES"
TARGET=$(LIBNAME) 
METALINK=$(LIBLINK) /out:$(LIBNAME)
!else
TARGET=$(DLLNAME) 
METALINK=$(DLLLINK) /pdb:$(PDBNAME) /implib:$(LIBNAME) /out:$(DLLNAME)
!endif
!endif

build:  info $(OBJDIR) $(BINDIR) $(TARGET)
rebuild: info $(OBJDIR) $(BINDIR) clean $(TARGET)

$(OBJDIR) $(LIBDIR) $(LIBDIR) $(BINDIR):
	@if not exist $@ md $@

info:
	@echo --------------------------------------------
	@echo .~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~
	@echo --------------------------------------------
	@echo build $(LINKAGE) $(CONFIG) $(PROJECT) on $(CPU) with VS$(COMPIGEN)
	@echo target is $(TARGET)
	@echo --------------------------------------------
