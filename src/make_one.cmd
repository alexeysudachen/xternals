
@echo off

call define_env

set ACTION=build
set DEBUG_SET=YES,NO
set STATIC_SET=YES,NO

set PROJECT=%1
shift

if .%PROJECT%. == .. goto :eof

:next_arg
if .%1. == .rebuild. set ACTION=rebuild
if .%1. == .clean. set ACTION=clean
if .%1. == .debug. set DEBUG_SET=YES
if .%1. == .release. set DEBUG_SET=NO
if .%1. == .static. set STATIC_SET=YES
if .%1. == .dynamic. set STATIC_SET=NO
if .%1. == .dll. set STATIC_SET=NO
if .%1. == .. goto :no_more
if .%1. == .13x86. set ENVIRONS=vs13
if .%1. == .10x86. set ENVIRONS=vs10
if .%1. == .13x64. set ENVIRONS=vs13_amd64
if .%1. == .10x64. set ENVIRONS=vs10_amd64
shift
goto :next_arg
:no_more

for %%n in (%ENVIRONS%) do (
	for %%d in (%DEBUG_SET%) do (
		for %%s in (%STATIC_SET%) do (
          			set DEBUG=%%d
          			set STATIC=%%s
          			cmd /c %%n build_target.cmd %ACTION%
		)
	)
)

